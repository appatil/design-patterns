package com.structural.proxy;

public class ProxyPatternDemo {

	public static void main(String args[]) {

		ProxyImage image = new ProxyImage("test.jpg");

		image.display();

		image.display();

	}
}
