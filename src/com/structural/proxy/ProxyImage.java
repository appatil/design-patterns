package com.structural.proxy;

public class ProxyImage implements Image {

	RealImage realImage = null;

	String fileName;

	public ProxyImage(String fileName) {
		super();
		this.fileName = fileName;
	}

	@Override
	public void display() {

		if (realImage == null) {
			realImage = new RealImage(fileName);
		} else {
			System.out.println(" Displaying Image  " + fileName);
		}

	}

}
