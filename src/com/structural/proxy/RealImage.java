package com.structural.proxy;

public class RealImage implements Image {

	String fileName;

	public RealImage(String fileName) {

		this.fileName = fileName;

		loadFromDisk();

		display();
	}

	@Override
	public void display() {

		System.out.println("  Displaying Image : " + fileName);
	}

	public void loadFromDisk() {

		System.out.println(" Loading From Disk ");
	}

}
