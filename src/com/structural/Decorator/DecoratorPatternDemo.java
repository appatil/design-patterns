package com.structural.Decorator;

public class DecoratorPatternDemo {

	public static void main(String args[]) {

		Circle circle = new Circle();

		RedShapeDecorator redCircle = new RedShapeDecorator(new Circle());

		RedShapeDecorator redRectangle = new RedShapeDecorator(new Rectangle());

		circle.draw();

		redCircle.draw();

		redRectangle.draw();

	}

}
