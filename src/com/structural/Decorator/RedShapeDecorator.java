package com.structural.Decorator;

public class RedShapeDecorator extends ShapeDecorator {

	public RedShapeDecorator(Shape shape) {
		super(shape);

	}

	public void setRedBorder() {
		System.out.println(" Border Color is Red ");
	}

	@Override
	public void draw() {
		shape.draw();
		setRedBorder();

	}
}
