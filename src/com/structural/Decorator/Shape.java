package com.structural.Decorator;

public interface Shape {

	public void draw();
}
