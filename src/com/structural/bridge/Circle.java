package com.structural.bridge;

public class Circle  extends Shape{
	
	private int x,y,radius;
	
	private DrawAPI drawAPI;
	
	
	public Circle(int x, int y,int radius, DrawAPI drawAPI)
	{
		super(drawAPI);
		this.x = x;
		this.y = y;
		this.radius = radius;
		this.drawAPI = drawAPI;
	}


	@Override
	public void draw() {
	
		drawAPI.draw(x,y,radius);
	}

}
