package com.structural.bridge;

public class GreenCircle  implements DrawAPI{

	@Override
	public void draw(int x, int y , int radius ) {
		System.out.println(" Drawing GREEN Circle " + " center " + x + "  "+y+" radius " + radius);
		
	}

}
