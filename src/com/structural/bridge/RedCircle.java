package com.structural.bridge;

public class RedCircle  implements DrawAPI{

	@Override
	public void draw(int x, int y , int radius ) {
		System.out.println(" Drawing RED Circle " + " center: " + x+ " " + y+" radius: " + radius);
		
		
	}

}
