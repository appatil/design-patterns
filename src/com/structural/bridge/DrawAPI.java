package com.structural.bridge;

public interface DrawAPI {

	public void draw(int x, int y, int r);
}
