package com.structural.adapter;

public class MediaAdapter implements MediaPlayer{

	
	private AdvancedMediaPlayer mediaPlayer;
	
	public void play(String record)
	{
		if(record.equals("VLC"))
		{
			mediaPlayer = new VLCPlayer();
			
			mediaPlayer.play();
		}
		
		if(record.equals("AVI"))
		{
			mediaPlayer = new AVIPlayer();
			
			mediaPlayer.play();
		}
	}
}
