package com.structural.adapter;

public class AdapterDemo {

	public static void main(String args[])
	{
		AudioPlayer player = new AudioPlayer();
		
		player.play("MP3");
		
		player.play("VLC");
		
		player.play("AVI");
	}
}
