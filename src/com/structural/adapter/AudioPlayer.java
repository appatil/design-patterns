package com.structural.adapter;

public class AudioPlayer  implements MediaPlayer{

	private MediaAdapter adapter = new MediaAdapter();
	@Override
	public void play(String record) {
		
		if(record.equals("MP3"))
	System.out.println("  Playing MP3 ");
		else
			adapter.play(record);
		
	}

}
