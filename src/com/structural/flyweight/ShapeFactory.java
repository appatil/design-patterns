package com.structural.flyweight;

import java.util.HashMap;

public class ShapeFactory {

	private static HashMap<String, Shape> circleCache = new HashMap<>();

	public static Circle getCircle(String color) {

		Circle circle = null;

		if (circleCache.containsKey(color)) {
			circle = (Circle) circleCache.get(color);

		}

		else {
			circle = new Circle();

			circle.setColor(color);

			circleCache.put(color, circle);
		}

		return circle;
	}

}
