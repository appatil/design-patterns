package com.structural.flyweight;

import java.util.Random;

public class FlyweightPatternDemo {

	static final String colors[] = { "Red", "Green", "Blue", "White", "Black" };

	static Random random = new Random();

	public static void main(String args[]) {

		ShapeFactory shapefactory = new ShapeFactory();

		for (int i = 0; i < 20; i++) {

			String color = getRandomColor();

			Circle circle = ShapeFactory.getCircle(color);

			circle.setX(getRandomInt());

			circle.setY(getRandomInt());

			circle.setRadius(getRandomInt());

			circle.draw();

		}

	}

	private static int getRandomInt() {
		return random.nextInt(100);
	}

	private static String getRandomColor() {

		return colors[random.nextInt(colors.length)];
	}
}
