package com.structural.flyweight;

public class Circle implements Shape {

	public int getX() {
		return x;
	}

	public void setX(int x) {
		this.x = x;
	}

	public int getY() {
		return y;
	}

	public void setY(int y) {
		this.y = y;
	}

	public int getRadius() {
		return radius;
	}

	public void setRadius(int radius) {
		this.radius = radius;
	}

	public String getColor() {
		return color;
	}

	public void setColor(String color) {
		this.color = color;
	}

	private int x;

	private int y;

	private int radius;

	private String color;

	public Circle(int x, int y, int radius, String color) {
		super();
		this.x = x;
		this.y = y;
		this.radius = radius;
		this.color = color;
	}

	public Circle() {
		super();
	}

	@Override
	public void draw() {

		System.out.println("  Drawing a circle " + "center : " + x + " , " + y
				+ " radius  : " + radius + " color  : " + color);
	}

}
