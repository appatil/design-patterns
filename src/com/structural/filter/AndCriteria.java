package com.structural.filter;

import java.util.List;

public class AndCriteria implements Criteria {

	private Criteria criteria;
	private Criteria otherCriteria;

	public AndCriteria(Criteria criteria, Criteria otherCriteria) {
		this.criteria = criteria;
		this.otherCriteria = otherCriteria;
	}

	@Override
	public List<Person> meetsCriteria(List<Person> persons) {
		List<Person> firstCriteriaPersons = criteria.meetsCriteria(persons);
		return otherCriteria.meetsCriteria(firstCriteriaPersons);
	}
}