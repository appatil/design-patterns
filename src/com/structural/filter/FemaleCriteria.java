package com.structural.filter;

import java.util.ArrayList;
import java.util.List;

public class FemaleCriteria implements Criteria {

	@Override
	public List<Person> meetsCriteria(List<Person> persons) {
		List<Person> femalePersons = new ArrayList<>();

		for (Person person : persons) {
			if (person.getGender().equals("Female")) {
				femalePersons.add(person);
			}
		}
		return femalePersons;
	}

}
