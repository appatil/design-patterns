package com.structural.filter;

import java.util.ArrayList;
import java.util.List;

public class SingleCriteria implements Criteria {

	@Override
	public List<Person> meetsCriteria(List<Person> persons) {
		List<Person> singlePersons = new ArrayList<>();

		for (Person person : persons) {
			if (person.getMartialStatus().equals("Single")) {
				singlePersons.add(person);
			}
		}
		return singlePersons;
	}
}
