package com.structural.filter;

import java.util.ArrayList;
import java.util.List;

public class MaleCriteria implements Criteria {

	@Override
	public List<Person> meetsCriteria(List<Person> persons) {
		List<Person> malePersons = new ArrayList<>();

		for (Person person : persons) {
			if (person.getGender().equals("Male")) {
				malePersons.add(person);
			}
		}
		return malePersons;
	}

}
