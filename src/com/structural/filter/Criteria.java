package com.structural.filter;

import java.util.List;

public interface Criteria {

	public List<Person> meetsCriteria(List<Person> persons);
}
