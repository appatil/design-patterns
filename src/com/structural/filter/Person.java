package com.structural.filter;

public class Person {

	String name;

	String gender;

	String martialStatus;

	public Person(String name, String gender, String martialStatus) {
		this.name = name;
		this.gender = gender;
		this.martialStatus = martialStatus;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getGender() {
		return gender;
	}

	public void setGender(String gender) {
		this.gender = gender;
	}

	public String getMartialStatus() {
		return martialStatus;
	}

	public void setMartialStatus(String martialStatus) {
		this.martialStatus = martialStatus;
	}

	@Override
	public String toString() {
		return "Person [name=" + name + ", gender=" + gender
				+ ", martialStatus=" + martialStatus + "]";
	}

}
