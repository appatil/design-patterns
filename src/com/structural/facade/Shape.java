package com.structural.facade;

public interface Shape {

	public void draw();
}
