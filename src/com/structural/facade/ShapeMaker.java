package com.structural.facade;

public class ShapeMaker {

	Shape circle;

	Shape square;

	Shape rectangle;

	public ShapeMaker() {

		circle = new Circle();

		rectangle = new Rectangle();

		square = new Square();
	}

	public void drawCircle() {

		circle.draw();
	}

	public void drawSquare() {

		square.draw();
	}

	public void drawRectangle() {

		rectangle.draw();
	}
}
