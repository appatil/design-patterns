package com.behavorial.intepretor;

public interface Expression {
	public boolean interpret(String context);
}