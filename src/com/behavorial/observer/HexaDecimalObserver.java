package com.behavorial.observer;

public class HexaDecimalObserver extends Observer {

	HexaDecimalObserver(Subject subject) {
		this.subject = subject;
		subject.attach(this);
	}

	@Override
	public void update() {
		System.out.println(" Octal String "
				+ Integer.toHexString(subject.getState()));

	}
}