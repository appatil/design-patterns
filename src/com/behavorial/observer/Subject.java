package com.behavorial.observer;

import java.util.ArrayList;

public class Subject {

	private int state;

	ArrayList<Observer> observers = new ArrayList<>();

	public void attach(Observer observer) {
		observers.add(observer);
	}

	public void notifyAllObservers() {

		for (Observer observer : observers) {
			observer.update();
		}
	}

	public int getState() {
		return state;
	}

	public void setState(int state) {
		this.state = state;
		notifyAllObservers();
	}
}
