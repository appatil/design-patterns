package com.behavorial.observer;

public class ObserverPatternDemo {

	public static void main(String args[]) {

		Subject subject = new Subject();

		Observer binaryObserver = new BinaryObserver(subject);

		Observer octalObserver = new OctalObserver(subject);

		Observer hexaDecimalObserver = new HexaDecimalObserver(subject);

		subject.setState(10);

		subject.setState(20);

		subject.setState(30);
	}
}
