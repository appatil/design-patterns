package com.behavorial.command;

public class BuyOrder implements Order {

	public BuyOrder(Stock stock) {
		super();
		this.stock = stock;
	}

	private Stock stock;

	@Override
	public void execute() {
		stock.buy();
	}

}
