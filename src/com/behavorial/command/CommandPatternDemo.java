package com.behavorial.command;

public class CommandPatternDemo {

	public static void main(String args[]) {

		Stock stock = new Stock();

		Order order1 = new BuyOrder(stock);

		Order order2 = new SellOrder(stock);

		Broker broker = new Broker();

		broker.addOrder(order1);

		broker.addOrder(order2);

		broker.placeOrders();

	}
}
