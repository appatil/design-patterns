package com.behavorial.command;

public interface Order {

	public void execute();
}
