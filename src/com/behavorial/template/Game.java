package com.behavorial.template;

public abstract class Game {

	public abstract void intialize();

	public abstract void start();

	public abstract void stop();

	public final void play() {

		intialize();

		start();

		stop();

	}

}
