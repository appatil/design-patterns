package com.behavorial.template;

public class Football extends Game {

	@Override
	public void intialize() {
		System.out.println("  Intialize Football ");

	}

	@Override
	public void start() {
		System.out.println("  Starting Football ");

	}

	@Override
	public void stop() {
		System.out.println("  Stopping Football ");

	}

}
