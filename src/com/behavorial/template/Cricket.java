package com.behavorial.template;

public class Cricket extends Game {

	@Override
	public void intialize() {
		System.out.println("  Intialize Cricket ");

	}

	@Override
	public void start() {
		System.out.println("  Starting Cricket ");

	}

	@Override
	public void stop() {
		System.out.println("  Stopping Cricket ");

	}

}
