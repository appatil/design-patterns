package com.creational.abstractfactory;

public class AbstractFactoryPatternDemo {

	public static void main(String args[]) {

		AbstractFactory factory = GetFactory.getFactory("Shape");

		Shape shape = factory.getShape("Circle");

		shape.draw();

		shape = factory.getShape("Rectangle");

		shape.draw();

		shape = factory.getShape("Square");

		shape.draw();

		factory = GetFactory.getFactory("Color");

		Color color = factory.getColor("Red");

		color.paint();

		color = factory.getColor("Green");

		color.paint();
	}
}
