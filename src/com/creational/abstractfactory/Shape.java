package com.creational.abstractfactory;

/**
 * The Interface Shape.
 */
public interface Shape {

	/**
	 * Draws the Shape.
	 */
	public void draw();

}
