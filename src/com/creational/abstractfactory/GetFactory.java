package com.creational.abstractfactory;

public class GetFactory {

	public static AbstractFactory getFactory(String typeOfFactory) {

		if (typeOfFactory == "Shape") {
			return new ShapeFactory();
		}

		else if (typeOfFactory == "Color") {
			return new ColorFactory();
		}

		return null;
	}
}
