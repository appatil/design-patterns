package com.creational.abstractfactory;

public class ColorFactory implements AbstractFactory {

	public Color getColor(String colorType) {
		if (colorType == "Red") {
			return new Red();
		}

		else if (colorType == "Green") {
			return new Green();
		}

		return null;
	}

	public Shape getShape(String shapeType) {

		return null;
	}
}
