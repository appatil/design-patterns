package com.creational.abstractfactory;

/**
 * The Class Circle. One of the object created and returned by the Shape
 * Factory.
 */
public class Circle implements Shape {

	/**
	 * Draws the name of the shape.
	 */
	public void draw() {
		System.out.println(" Drawing a Circle ");

	}

}
