package com.creational.abstractfactory;

// TODO: Auto-generated Javadoc
/**
 * The Class Square.
 */
public class Square implements Shape {

	/* (non-Javadoc)
	 * @see com.factorypattern.Shape#draw()
	 */
	public void draw() {
		System.out.println(" Drawing a Square ");

	}

}
