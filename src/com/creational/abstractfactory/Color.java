package com.creational.abstractfactory;

/**
 * The Interface Color.
 */
public interface Color {

	/**
	 * Paints the Color.
	 */
	public void paint();
}
