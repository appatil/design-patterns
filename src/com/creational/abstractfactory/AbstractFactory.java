package com.creational.abstractfactory;

/**
 * A factory for creating Abstract objects.
 * 
 * @author aniket22
 * 
 */
public interface AbstractFactory {

	/**
	 * Gets the shape.
	 * 
	 * @return the shape
	 */
	public Shape getShape(String shapeType);

	/**
	 * Gets the color.
	 * 
	 * @return the color
	 */
	public Color getColor(String colorType);

}
