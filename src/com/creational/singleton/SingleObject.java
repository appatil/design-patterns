package com.creational.singleton;

public class SingleObject {

	// create a single instance of the class.
	private static SingleObject singleObject = new SingleObject();

	// make the constructor private
	private SingleObject() {

	}

	// return the instance by a static method.
	public static SingleObject getInstance() {
		return singleObject;
	}

}
