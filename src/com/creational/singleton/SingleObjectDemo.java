package com.creational.singleton;

public class SingleObjectDemo {

	public static void main(String args[]) {

		SingleObject singleObject = SingleObject.getInstance();

		System.out.println(singleObject);
	}
}
