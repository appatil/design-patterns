package com.creational.builder;

public interface Packing {
	
	public String pack();

}
