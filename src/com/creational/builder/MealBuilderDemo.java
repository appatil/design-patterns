package com.creational.builder;

public class MealBuilderDemo {

	public static void main(String args[])
	{
	MealBuilder mealBuilder = new MealBuilder();
	
	
	Meal meal = null;
	
	meal = mealBuilder.buildNonVegMeal();
	
	meal.showItems();
	
	meal = mealBuilder.buildVegMeal();
	
	meal.showItems();
	}
	
}
