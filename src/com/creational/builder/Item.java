package com.creational.builder;

public interface Item {
	
public String name();

public double price();

public Packing packing();
	
}
