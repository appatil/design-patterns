package com.creational.builder;

import java.util.ArrayList;

public class Meal {

	private ArrayList<Item> items = new ArrayList<Item>();
	
	public void addItem(Item item)
	{
		items.add(item);
	}
	
	public double getCost(){
		
		double cost = 0.0;
		for(Item item : items)
		{
			cost += item.price();
		}
		
		return cost;
	}
	
	public void showItems()
	{
		for(Item item : items)
		{
			System.out.println("Item : " + item.name() + item.price() + item.packing().pack());
		}
	
	}
}
