package com.creational.factorypattern;

/**
 * The Class Rectangle. One of the object created by the Shape Factory
 */
public class Rectangle implements Shape {

	/**
	 * Draws the rectangle shape.
	 */
	public void draw() {
		System.out.println(" Drawing a Rectangle ");

	}

}
