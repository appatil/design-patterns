package com.creational.factorypattern;

/**
 * The Class ShapeFactoryDemo.
 */
public class ShapeFactoryDemo {

	/**
	 * The main method.
	 * 
	 * @param args
	 *            the arguments
	 */
	public static void main(String args[]) {

		ShapeFactory shapeFactory = new ShapeFactory();

		Shape shape = shapeFactory.getShape("Circle");

		shape.draw();

		shape = shapeFactory.getShape("Rectangle");

		shape.draw();

		shape = shapeFactory.getShape("Square");

		shape.draw();
	}
}
