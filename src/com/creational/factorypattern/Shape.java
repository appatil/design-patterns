package com.creational.factorypattern;

/**
 * The Interface Shape.
 */
public interface Shape {

	/**
	 * Absract method implemented by the objects.
	 */
	public void draw();
}
