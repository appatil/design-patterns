package com.creational.factorypattern;


/**
 * A factory for creating Shape objects.
 */
public class ShapeFactory {

	/**
	 * Gets the shape.
	 * 
	 * @param shapeType
	 *            the shape type
	 * @return the shape
	 */

	private final String CIRCLE = "Circle";

	private final String RECTANGLE = "Rectangle";

	private final String SQUARE = "Square";

	public Shape getShape(String shapeType) {

		if (shapeType == CIRCLE) {
			return new Circle();
		} else if (shapeType == RECTANGLE) {
			return new Rectangle();
		} else if (shapeType == SQUARE) {
			return new Square();
		}

		return null;
	}

}
