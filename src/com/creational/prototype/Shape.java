package com.creational.prototype;

public abstract class Shape implements Cloneable{

	public abstract void draw();
	
	  public Shape clone() {
	      Object clone = null;
	      try {
	         clone = super.clone();
	      } catch (CloneNotSupportedException e) {
	         e.printStackTrace();
	      }
	      return (Shape)clone;
	   }
}
