package com.creational.prototype;

public class PrototypeDemo {

	public static void main(String args[])
	{
		ShapeCache cache = new ShapeCache();
		
		cache.getShape(1).draw();
	    
	    cache.getShape(2).draw();
		
        cache.getShape(3).draw();
	}
}
