package com.creational.prototype;

import java.util.HashMap;

public class ShapeCache {

	
	private static HashMap<Integer,Shape> shapeCache = new HashMap<>();
	
	static {
		
		shapeCache.put(1, new Circle());
		
		shapeCache.put(2, new Rectangle());
		
		shapeCache.put(3, new Square());
		
	}
	
	public Shape getShape(int shapeId)
	{
		if(shapeCache.containsKey(shapeId))
		{
			return  shapeCache.get(shapeId).clone();
		}
		
		return null;
	}
}
