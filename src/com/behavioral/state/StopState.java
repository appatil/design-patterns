package com.behavioral.state;

public class StopState extends State {

	@Override
	public void doAction(Context context) {

		System.out.println("  In the Stop State ");

		context.setState(this);

	}

	@Override
	public String toString() {
		return "Stop  State ";
	}

}