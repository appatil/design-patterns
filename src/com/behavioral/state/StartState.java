package com.behavioral.state;

public class StartState extends State {

	@Override
	public String toString() {
		return "Start State ";
	}

	@Override
	public void doAction(Context context) {

		System.out.println("  In the Starting State ");

		context.setState(this);

	}

}
