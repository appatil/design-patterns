package com.behavioral.state;

public abstract class State {

	public abstract void doAction(Context context);
}
