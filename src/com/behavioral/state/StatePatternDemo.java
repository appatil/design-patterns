package com.behavioral.state;

public class StatePatternDemo {

	public static void main(String args[]) {

		State state = null;

		Context context = new Context();

		state = new StartState();

		state.doAction(context);

		System.out.println(context.getState());

		state = new StopState();

		state.doAction(context);

		System.out.println(context.getState());

	}
}
