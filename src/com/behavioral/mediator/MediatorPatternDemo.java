package com.behavioral.mediator;

public class MediatorPatternDemo {

	public static void main(String args[]) {
		User user1 = new User("aniket");

		User user2 = new User("patil");

		user1.sendMessage(" Wassup Dude ");

		user2.sendMessage(" Coding Dude ");
	}

}
