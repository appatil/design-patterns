package com.behavioral.memonto;

import java.util.ArrayList;

public class CareTaker {

	private ArrayList<Memento> lists = new ArrayList<>();

	public void addMemento(Memento memento) {

		lists.add(memento);
	}

	public Memento getMemento(int index) {

		return lists.get(index);
	}
}
