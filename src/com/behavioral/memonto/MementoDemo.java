package com.behavioral.memonto;

public class MementoDemo {

	public static void main(String args[]) {

		Originator originator = new Originator(" State 1");

		CareTaker careTaker = new CareTaker();

		originator.setState(" State 2 ");

		careTaker.addMemento(originator.saveStateToMemonto());

		originator.setState(" State 3 ");

		careTaker.addMemento(originator.saveStateToMemonto());

		originator.setState(" State 4 ");

		System.out.println("  Current State " + originator.getState());

		originator.getStateFromMemento(careTaker.getMemento(1));

		System.out.println(" Previous State " + originator.getState());

		originator.getStateFromMemento(careTaker.getMemento(0));

		System.out.println(" Two State Before  " + originator.getState());

	}
}
