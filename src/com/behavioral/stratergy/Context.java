package com.behavioral.stratergy;

public class Context {

	private Stratergy stratergy;

	public Context(Stratergy stratergy) {
		super();
		this.stratergy = stratergy;
	}

	public int executeStratergy(int num1, int num2) {

		return stratergy.doOperation(num1, num2);
	}
}
