package com.behavioral.stratergy;

public class StratergyPatternDemo {

	public static void main(String args[]) {

		Context context = new Context(new AddOperation());

		System.out.println("" + context.executeStratergy(10, 5));

		context = new Context(new SubtractOperation());

		System.out.println("" + context.executeStratergy(10, 5));

		context = new Context(new MultiplyOperation());

		System.out.println("" + context.executeStratergy(10, 5));
	}
}
