package com.behavioral.stratergy;

public class SubtractOperation implements Stratergy {

	@Override
	public int doOperation(int number1, int number2) {

		return Math.abs(number1 - number2);
	}

}
