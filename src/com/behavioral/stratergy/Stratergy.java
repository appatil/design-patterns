package com.behavioral.stratergy;

public interface Stratergy {

	public int doOperation(int number1, int number2);

}
