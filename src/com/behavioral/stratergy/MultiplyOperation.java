package com.behavioral.stratergy;

public class MultiplyOperation implements Stratergy {

	@Override
	public int doOperation(int number1, int number2) {

		return number1 * number2;
	}

}
