package com.behavioral.nullpattern;

public class NullPatternDemo {

	public static void main(String args[]) {

		AbstractCustomer customer = CustomerFactory.getCustomer("aniket");

		System.out.println(customer.getName());

		customer = CustomerFactory.getCustomer("patil");

		System.out.println(customer.getName());
	}
}
