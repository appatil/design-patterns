package com.behavioral.nullpattern;

public class CustomerFactory {

	private static String customerNames[] = { "aniket", "anamika", "priyanka",
			"swati" };

	public static AbstractCustomer getCustomer(String customerName) {
		for (int i = 0; i < customerNames.length; i++) {

			if (customerName.equals(customerNames[i])) {
				return new RealCustomer(customerName);

			}
		}

		return new NullCustomer();
	}
}
