package com.behavioral.chainofresponsibilities;

public class ChainPatternDemo {

	public static AbstractLogger getChain() {

		AbstractLogger errorLogger = new ErrorLogger(AbstractLogger.ERROR);
		AbstractLogger fileLogger = new FileLogger(AbstractLogger.DEBUG);
		AbstractLogger consoleLogger = new ConsoleLogger(AbstractLogger.INFO);

		errorLogger.setNextLogger(fileLogger);
		fileLogger.setNextLogger(consoleLogger);

		return errorLogger;
	}

	public static void main(String args[]) {

		AbstractLogger abstractLogger = getChain();

		abstractLogger.logMessage(AbstractLogger.INFO,
				"This is an information message");

		abstractLogger.logMessage(AbstractLogger.DEBUG,
				"This is an debug message");

		abstractLogger.logMessage(AbstractLogger.ERROR,
				"This is an error message");

	}
}
