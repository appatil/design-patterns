package com.behavioral.iterator;

public class IteratorPatternDemo {

	public static void main(String args[]) {

		NamesRepository namesRepository = new NamesRepository();

		Iterator iterator = namesRepository.getIterator();

		while (iterator.hasNext()) {
			System.out.println(iterator.next());
		}
	}
}
