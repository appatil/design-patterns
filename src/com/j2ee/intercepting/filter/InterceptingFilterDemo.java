package com.j2ee.intercepting.filter;

public class InterceptingFilterDemo {

	public static void main(String args[]) {

		FilterManager filterManager = new FilterManager(new Target());

		filterManager.setFilter(new AuthenticatingFilter());

		filterManager.setFilter(new LoggingFilter());

		Client client = new Client();

		client.setFilterManager(filterManager);

		client.sendRequest("LOGIN");

	}

}
