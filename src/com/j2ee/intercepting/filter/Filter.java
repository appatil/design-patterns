package com.j2ee.intercepting.filter;

public interface Filter {

	public void execute(String request);

}
