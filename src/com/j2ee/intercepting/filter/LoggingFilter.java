package com.j2ee.intercepting.filter;

public class LoggingFilter implements Filter {

	@Override
	public void execute(String request) {
		System.out.println("  Logging Request  " + request);

	}

}
