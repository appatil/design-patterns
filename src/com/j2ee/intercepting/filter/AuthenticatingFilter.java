package com.j2ee.intercepting.filter;

public class AuthenticatingFilter implements Filter {

	@Override
	public void execute(String request) {
		System.out.println("  Authenticating Request  " + request);

	}

}
