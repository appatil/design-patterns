package com.j2ee.mvc;

public class StudentView {

	public void printStudentDetails(Student student) {

		System.out.println(" Student Name : " + student.getName());

		System.out.println(" Student Roll Number : " + student.getRollNumber());
	}
}
