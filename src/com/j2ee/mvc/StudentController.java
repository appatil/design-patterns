package com.j2ee.mvc;

public class StudentController {

	private Student student;

	private StudentView studentView;

	public StudentController(Student student, StudentView studentView) {
		super();
		this.student = student;
		this.studentView = studentView;
	}

	public static Student getStudentFromDatabase() {
		return new Student("aniket", 1205068744);

	}

	public Student getStudent() {
		return student;
	}

	public void setStudent(Student student) {
		this.student = student;
	}

	public StudentView getStudentView() {
		return studentView;
	}

	public void setStudentView(StudentView studentView) {
		this.studentView = studentView;
	}

	public void updateView() {
		studentView.printStudentDetails(student);
	}
}
