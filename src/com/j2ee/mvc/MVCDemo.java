package com.j2ee.mvc;

public class MVCDemo {

	public static void main(String args[]) {

		Student student = StudentController.getStudentFromDatabase();

		StudentView studentView = new StudentView();

		StudentController controller = new StudentController(student,
				studentView);

		controller.updateView();

		student.setName("appatil");

		controller.updateView();

	}
}
