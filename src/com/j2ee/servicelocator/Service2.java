package com.j2ee.servicelocator;

public class Service2 implements Service {

	String serviceName;

	public Service2() {
		serviceName = "Service1";
	}

	@Override
	public String getName() {

		return serviceName;
	}

	@Override
	public void execute() {
		System.out.println("  Executing Service 2 ");

	}

}
