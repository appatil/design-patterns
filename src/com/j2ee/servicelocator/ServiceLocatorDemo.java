package com.j2ee.servicelocator;

public class ServiceLocatorDemo {

	public static void main(String args[]) {

		Service service = ServiceLocator.getService("Service1");

		service.execute();

		service = ServiceLocator.getService("Service1");

		service.execute();

	}
}
