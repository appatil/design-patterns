package com.j2ee.servicelocator;

import java.util.ArrayList;
import java.util.List;

public class Cache {

	private List<Service> servicesCache = new ArrayList<>();

	public Service getService(String serviceName) {

		for (Service service : servicesCache) {
			if (service.getName().equals(serviceName)) {
				return service;
			}
		}

		return null;
	}

	public void addService(Service service1) {
		servicesCache.add(service1);
	}

}
