package com.j2ee.servicelocator;

public class Service1 implements Service {

	String serviceName;

	public Service1() {
		serviceName = "Service1";
	}

	@Override
	public String getName() {
		// TODO Auto-generated method stub
		return serviceName;
	}

	@Override
	public void execute() {
		System.out.println("  Executing Service 1 ");

	}

}
