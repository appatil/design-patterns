package com.j2ee.servicelocator;

public class InitialContext {

	public Object lookUp(String serviceName) {

		if (serviceName.equals("Service1")) {

			System.out.println("  Looking Up Service 1 ");
			return new Service1();
		}

		else if (serviceName.equals("Service2")) {

			System.out.println("  Looking Up Service 2 ");
			return new Service2();
		}
		return null;

	}
}
