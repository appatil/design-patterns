package com.j2ee.dataaccessobject;

import java.util.List;

public class DataAccessObjectDemo {

	public static void main(String args[]) {

		StudentDAO studentDAO = new StudentDAOImpl();

		studentDAO.addStudent(new Student("aniket", 10));

		studentDAO.addStudent(new Student("patil", 22));

		List<Student> students = studentDAO.getStudents();

		for (Student student : students) {
			System.out.println(student);
		}

		// studentDAO.updateStudent(new Student("appatil", 10));

		for (Student student : students) {
			System.out.println(student);
		}

		studentDAO.removeStudent(22);

		for (Student student : students) {
			System.out.println(student);
		}

	}
}
