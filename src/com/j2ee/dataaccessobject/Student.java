package com.j2ee.dataaccessobject;

public class Student {

	private String name;

	@Override
	public String toString() {
		return "Student [name=" + name + ", id=" + id + "]";
	}

	private int id;

	public Student(String name, int id) {
		super();
		this.name = name;
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

}
