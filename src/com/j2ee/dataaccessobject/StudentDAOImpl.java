package com.j2ee.dataaccessobject;

import java.util.ArrayList;
import java.util.List;

public class StudentDAOImpl implements StudentDAO {

	List<Student> students = new ArrayList<>();

	@Override
	public List<Student> getStudents() {

		return students;
	}

	@Override
	public void addStudent(Student student) {
		students.add(student);
	}

	@Override
	public void updateStudent(Student student) {

		students.get(student.getId()).setName(student.getName());
	}

	@Override
	public void removeStudent(int id) {
		for (Student student : students) {
			if (student.getId() == id) {
				students.remove(student);
				break;
			}
		}
	}

}
