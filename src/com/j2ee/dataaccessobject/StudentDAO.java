package com.j2ee.dataaccessobject;

import java.util.List;

public interface StudentDAO {

	public List<Student> getStudents();

	public void addStudent(Student student);

	public void updateStudent(Student student);

	public void removeStudent(int id);
}
