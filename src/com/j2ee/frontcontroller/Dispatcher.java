package com.j2ee.frontcontroller;

public class Dispatcher {

	private StudentView studentView = new StudentView();

	private HomeView homeView = new HomeView();

	public void dispatchRequest(String request) {
		if (request.equals("HOME")) {
			studentView.display();
		}

		else if (request.equals("STUDENT")) {
			homeView.display();
		}
	}
}
