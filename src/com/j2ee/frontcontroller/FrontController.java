package com.j2ee.frontcontroller;

public class FrontController {

	private Dispatcher dispatcher = new Dispatcher();

	public void trackRequest() {

		System.out.println("  Tracking request ");

	}

	public boolean authenticateUser() {

		System.out.println("  Authenticating User  ");

		return true;

	}

	public void dispatchRequest(String request) {

		trackRequest();

		authenticateUser();

		dispatcher.dispatchRequest(request);

	}

}
