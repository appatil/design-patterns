package com.j2ee.business.service;

public class BusinessDelegate {

	BusinessServiceLookup serviceLookup = new BusinessServiceLookup();

	BusinessService businessService;

	public BusinessDelegate(String serviceType) {
		super();
		this.serviceType = serviceType;
	}

	private String serviceType;

	public String getServiceType() {
		return serviceType;
	}

	public void setServiceType(String serviceType) {
		this.serviceType = serviceType;
	}

	public void doTask() {
		businessService = serviceLookup.lookUpService(serviceType);
		businessService.process();
	}
}
