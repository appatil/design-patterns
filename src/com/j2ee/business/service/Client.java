package com.j2ee.business.service;

public class Client {

	private BusinessDelegate businessDelegate;

	public Client(BusinessDelegate businessDelegate) {
		super();
		this.businessDelegate = businessDelegate;
	}

	public void dotask() {
		businessDelegate.doTask();
	}

}
