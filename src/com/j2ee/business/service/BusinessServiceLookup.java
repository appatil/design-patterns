package com.j2ee.business.service;

public class BusinessServiceLookup {

	public BusinessService lookUpService(String serviceType) {

		if (serviceType.equals("EJB")) {
			return new EJBService();
		} else if (serviceType.equals("JMS")) {
			return new JMSService();
		}
		return null;
	}
}
