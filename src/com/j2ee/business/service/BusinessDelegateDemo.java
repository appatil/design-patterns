package com.j2ee.business.service;

public class BusinessDelegateDemo {

	public static void main(String args[]) {

		BusinessDelegate bizDelegate = new BusinessDelegate("EJB");

		Client client = new Client(bizDelegate);

		client.dotask();

	}

}
