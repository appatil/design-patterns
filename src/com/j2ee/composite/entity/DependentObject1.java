package com.j2ee.composite.entity;

public class DependentObject1 {

	String data;

	public String getData() {
		return data;
	}

	public void setData(String data) {
		this.data = data;
	}

}
