package com.j2ee.composite.entity;

public class Client {
	private CompositeEntity compositeEntity = new CompositeEntity();

	public void printData() {

		System.out.println("Data: " + compositeEntity.getData());

	}

	public void setData(String data1, String data2) {
		compositeEntity.setData(data1, data2);
	}
}
