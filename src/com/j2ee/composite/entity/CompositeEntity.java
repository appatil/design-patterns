package com.j2ee.composite.entity;

public class CompositeEntity {

	CoarseGrainObject cgo = new CoarseGrainObject();

	public void setData(String data1, String data2) {
		cgo.setData(data1, data2);
	}

	public String getData() {

		return cgo.getData();
	}

}
